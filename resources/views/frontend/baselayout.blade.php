<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Eminent</title>
    <!-- Bootstrap -->
    <link href="{{ asset('frontend/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/ion-icons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/construction-fonts/flaticon.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/owl-carousel/assets/owl.carousel.min.css') }}" rel="stylesheet">
    <link href="{{ asset('frontend/owl-carousel/assets/owl.theme.default.min.css') }}" rel="stylesheet">

    <!--master slider-->
    <link rel="stylesheet" href="{{ asset('frontend/masterslider/style/masterslider.css') }}"/>
    <link href="{{ asset('frontend/masterslider/skins/default/style.css') }}" rel='stylesheet' type='text/css'>
    <link href="{{ asset('frontend/cubeportfolio/css/cubeportfolio.min.css') }}" rel="stylesheet">
    <link rel='stylesheet' type="text/css" href="{{ asset('frontend/dzsparallaxer/dzsparallaxer.css') }}">
    <link href="{{ asset('frontend/css/style.css') }}" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
{{--<div id="preloader"></div>--}}
<!--top bar-->
<div class="top-bar">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <span><i class="ion-android-call"></i> +01 555-555-555</span>
                <span><i class="ion-ios-email"></i> info@eminentedu.com</span>
            </div>
            <div class="col-lg-6">
                <ul class="list-inline text-right margin-btm-0">
                    <li class="list-inline-item"><a href="#"><i class="ion-social-facebook"></i></a></li>
                    <li class="list-inline-item"><a href="#"><i class="ion-social-twitter"></i></a></li>
                    <li class="list-inline-item"><a href="#"><i class="ion-social-googleplus"></i></a></li>
                    <li class="list-inline-item"><a href="#"><i class="ion-social-linkedin"></i></a></li>
                    <li class="list-inline-item"><a href="#"><i class="ion-social-instagram"></i></a></li>
                </ul>
            </div>

        </div>
    </div>
</div>
<!--top bar end-->

<!-- navbar -->
<nav class="navbar navbar-expand-lg navbar-light bg-white">
    <div class="container">
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="index.html"><img src="images/logo.png" alt=""></a>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav ml-auto">
                <li class="dropdown nav-item active">
                    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Home <span class="caret"></span></a>
                    <ul class="dropdown-menu dropdown-menu-left">
                        <li><a href="index.html" class="dropdown-item">V1 - Default Slider</a></li>
                        <li><a href="index-static-img.html" class="dropdown-item">V2 - Static Image</a></li>
                        <li><a href="index-youtbe.html" class="dropdown-item">V3 - Youtube background</a>
                        </li><li><a href="index-onepage.html" class="dropdown-item">one  page</a></li>
                        <li class="dropdown-submenu">
                            <a tabindex="-1" href="#" class="dropdown-item">headers </a>
                            <ul class="dropdown-menu">
                                <li><a href="index.html" class="dropdown-item"> Default</a></li>
                                <li><a href="header-transparent.html" class="dropdown-item"> Transparent</a></li>
                                <li><a href="header-sticky.html" class="dropdown-item"> Sticky</a></li>
                                <li><a href="header-dark.html" class="dropdown-item"> Dark</a></li>
                                <li class="dropdown-submenu">
                                    <a tabindex="-1" href="#" class="dropdown-item">menu level 2 </a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#" class="dropdown-item"> menu level 3</a></li>
                                        <li><a href="#" class="dropdown-item"> menu level 3</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-item"><a href="about.html" class="nav-link">About</a></li>
                <li class="dropdown nav-item">
                    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Projects <span class="caret"></span></a>
                    <ul class="dropdown-menu dropdown-menu-left">
                        <li><a href="projects-2col.html" class="dropdown-item">2 Columns</a></li>
                        <li><a href="projects.html" class="dropdown-item">3 Columns</a></li>
                        <li><a href="projects-4col.html" class="dropdown-item">4 Columns</a></li>
                        <li><a href="projects-full-width.html" class="dropdown-item">Full width</a></li>
                        <li><a href="single-project.html" class="dropdown-item">Single Project</a></li>
                    </ul>
                </li>
                <li class="dropdown nav-item">
                    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Services <span class="caret"></span></a>
                    <ul class="dropdown-menu dropdown-menu-left">
                        <li><a href="service.html" class="dropdown-item">All Services</a></li>
                        <li><a href="renovation-service.html" class="dropdown-item">House renovation</a></li>
                        <li><a href="management-service.html" class="dropdown-item">Project management</a></li>
                        <li><a href="construction-service.html" class="dropdown-item">Construction</a></li>
                        <li><a href="interior-service.html" class="dropdown-item">Interior design</a></li>
                    </ul>
                </li>
                <li class="dropdown nav-item">
                    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Features <span class="caret"></span></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="typography.html" class="dropdown-item">Typography</a></li>
                        <li><a href="tabs.html" class="dropdown-item">Tabs</a></li>
                        <li><a href="accordoins.html" class="dropdown-item">Accordions</a></li>
                        <li><a href="grid-system.html" class="dropdown-item">Grid System</a></li>
                    </ul>
                </li>
                <li class="dropdown nav-item">
                    <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Pages <span class="caret"></span></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <li><a href="blog.html" class="dropdown-item">Blog</a></li>
                        <li><a href="post.html" class="dropdown-item">Blog Post</a></li>
                        <li><a href="contact.html" class="dropdown-item">Contact</a></li>

                        <li><a href="gallery.html" class="dropdown-item">Gallery</a></li>
                        <li><a href="error-404.html" class="dropdown-item">Error-404</a></li>
                        <li><a href="full-width.html" class="dropdown-item">Page Full Width</a></li>
                        <li><a href="page-right-sidebar.html" class="dropdown-item">Page Right Sidebar</a></li>
                        <li><a href="page-left-sidebar.html" class="dropdown-item">Page Left Sidebar</a></li>

                    </ul>
                </li>
                <li class="search-toggle nav-item"><a href="javascript:void(0)" class="nav-link"><i class="ion-search"></i></a>
                    <form class="search-form">
                        <input type="text" class="form-control" placeholder="Search here...">
                        <button type="submit" style="display: none">Search</button>
                    </form>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div><!--/.container -->
</nav><!--/nav -->





@yield('content')

{{-- body here--}}



<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 margin-btm-30">
                <a href="index.html"><img src="images/logo-light.png" alt=""></a>
                <div class="space30"></div>
                <p>
                    Sed ut perspiciatis unde omnis doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                </p>
                <a href="#" class="btn btn-link">Read More</a>
            </div>
            <div class="col-lg-3 margin-btm-30">
                <h3>Our Services</h3>
                <ul class="list-unstyled">
                    <li><a href="#">Interior Designs</a></li>
                    <li><a href="#">Project Management</a></li>
                    <li><a href="#">House Renovation</a></li>
                    <li><a href="#">Constructions</a></li>
                </ul>
            </div>
            <div class="col-lg-5">
                <h3>Latest Projects</h3>
                <div class="latest-projects">
                    <a href="#">
                        <img src="images/work-1.jpg" alt="" class="img-fluid">
                    </a>
                    <a href="#">
                        <img src="images/work-2.jpg" alt="" class="img-fluid">
                    </a>
                    <a href="#">
                        <img src="images/work-3.jpg" alt="" class="img-fluid">
                    </a>
                    <a href="#">
                        <img src="images/work-4.jpg" alt="" class="img-fluid">
                    </a>
                    <a href="#">
                        <img src="images/work-5.jpg" alt="" class="img-fluid">
                    </a>
                    <a href="#">
                        <img src="images/work-6.jpg" alt="" class="img-fluid">
                    </a>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-lg-7">
                <ul class="list-inline">
                    <li class="list-inline-item"><a href="#">Home</a></li>
                    <li class="list-inline-item"><a href="#">About</a></li>
                    <li class="list-inline-item"><a href="#">Services</a></li>
                    <li class="list-inline-item"><a href="#">Blog</a></li>
                    <li class="list-inline-item"><a href="#">Terms & Conditions</a></li>
                </ul>
            </div>
            <div class="col-lg-5 text-right">
                <span>&copy; 2017. All Right Reserved. Eminent</span>
            </div>
        </div>
    </div>
</footer>
<!--footer end-->

<!--back to top-->
<a href="#" class="scrollToTop"><i class="ion-android-arrow-dropup-circle"></i></a>
<!--back to top end-->

<!-- jQuery -->
<script src="{{ asset('frontend/js/jquery.min.js') }}"></script>
<script src="{{ asset('frontend/js/jquery-migrate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('frontend/bootstrap/popper.min.js') }}"></script>
<script src="{{ asset('frontend/bootstrap/dist/js/bootstrap.min.js') }}"></script>

<script src="{{ asset('frontend/js/wow.min.js') }}"></script>
<script src="{{ asset('frontend/dzsparallaxer/dzsparallaxer.js') }}" type="text/javascript"></script>

<script src="{{ asset('frontend/owl-carousel/owl.carousel.min.js') }}"></script>
<script src="{{ asset('frontend/js/custom.js') }}"></script>
<!--page template scripts-->
<script src="masterslider/masterslider.min.js"></script>
<script src="js/master-custom.js"></script>
<!--page script-->
<script src="cubeportfolio/js/jquery.cubeportfolio.min.js"></script>
<script>
    (function ($, window, document, undefined) {
        'use strict';

        // init cubeportfolio
        $('#js-grid-agency').cubeportfolio({
            filters: '#js-filters-agency',
            loadMore: '#js-loadMore-agency',
            loadMoreAction: 'click',
            layoutMode: 'grid',
            defaultFilter: '*',
            animationType: 'slideLeft',
            gapHorizontal: 15,
            gapVertical: 0,
            gridAdjustment: 'responsive',
            mediaQueries: [{
                width: 1500,
                cols: 4
            }, {
                width: 1100,
                cols: 4
            }, {
                width: 800,
                cols: 3
            }, {
                width: 480,
                cols: 2
            }, {
                width: 320,
                cols: 1
            }],
            caption: 'zoom',
            displayType: 'fadeIn',
            displayTypeSpeed: 100
        });
    })(jQuery, window, document);
</script>

</body>

</html>