@extends('frontend.baselayout')
@section('content')

    <div class="clearfix"></div>
    <div class="slider-main" style="overflow: hidden;">
        <!-- Master Slider -->
        <div class="master-slider ms-skin-default" id="masterslider">

            <!-- slide 1 -->
            <div class="ms-slide slide-1" data-delay="14">

                <!-- slide background -->
                <img src="masterslider/style/blank.gif" data-src="images/bg-1.jpg" alt="Slide1 background"/>
                <h3 class="ms-layer sub-title3 text-center white text-uppercase"
                    style="left:150px; top:200px;"
                    data-type="text"
                    data-delay="1000"
                    data-duration="2000"
                    data-ease="easeOutExpo"
                    data-effect="rotate3dtop(-100,0,0,40,t)">Bpress</h3>
                <h3 class="ms-layer title2 text-uppercase text-center"
                    style="left:150px;top: 260px;"
                    data-type="text"
                    data-delay="1500"
                    data-duration="2500"
                    data-ease="easeOutExpo"
                    data-effect="rotate3dtop(-100,0,0,40,t)">Best Construction Template</h3>
                <h5 class="ms-layer sub-title2 text-uppercase"
                    style="left:150px; top: 330px;"
                    data-type="text"
                    data-effect="bottom(45)"
                    data-duration="3000"
                    data-delay="2000"
                    data-ease="easeOutExpo">Development dolor sit amet consectetur adipiscing Phasellus</h5>
                <a class="ms-layer btn1" href="#"
                   style="left: 150px; top:380px;"
                   data-type="text"
                   data-delay="2500"
                   data-ease="easeOutExpo"
                   data-duration="2000"
                   data-effect="scale(1.5,1.6)"> Learn More <i class="ion-ios-arrow-right"></i> </a>
                <a class="ms-layer btn2" href="#"
                   style="left: 320px; top:380px;"
                   data-type="text"
                   data-delay="3000"
                   data-ease="easeOutExpo"
                   data-duration="2200"
                   data-effect="scale(1.5,1.6)"> Purchase Now! <i class="ion-ios-arrow-right"></i></a> </div>
            <!-- end of slide -->

            <!-- slide 2 -->
            <div class="ms-slide slide-2" data-delay="14">

                <!-- slide background -->
                <img src="masterslider/style/blank.gif" data-src="images/bg-2.jpg" alt="Slide1 background"/>
                <h3 class="ms-layer sub-title3 text-uppercase"
                    style="left:110px; top:205px;"
                    data-type="text"
                    data-delay="1000"
                    data-duration="2000"
                    data-ease="easeOutExpo"
                    data-effect="rotate3dtop(-100,0,0,40,t)">We build</h3>
                <h3 class="ms-layer title2  text-uppercase"
                    style="left:110px;top: 260px;"
                    data-type="text"
                    data-delay="1500"
                    data-duration="2500"
                    data-ease="easeOutExpo"
                    data-effect="rotate3dtop(-100,0,0,40,t)">Awesome projects</h3>
                <h5 class="ms-layer sub-title2 text-uppercase"
                    style="left:110px; top:330px;"
                    data-type="text"
                    data-effect="bottom(45)"
                    data-duration="3000"
                    data-delay="2000"
                    data-ease="easeOutExpo">Development dolor sit amet consectetur adipiscing Phasellus</h5>
                <a class="ms-layer btn1" href="#"
                   style="left:110px; top:380px;"
                   data-type="text"
                   data-delay="2500"
                   data-ease="easeOutExpo"
                   data-duration="2000"
                   data-effect="scale(1.5,1.6)"> Learn More <i class="ion-ios-arrow-right"></i>
                </a> <a class="ms-layer btn2" href="#"
                        style="left:280px; top:380px;"
                        data-type="text"
                        data-delay="3000"
                        data-ease="easeOutExpo"
                        data-duration="2200"
                        data-effect="scale(1.5,1.6)"> Purchase Now! <i class="ion-ios-arrow-right"></i></a> </div>
            <!-- end of slide -->

            <!-- slide 1 -->
            <div class="ms-slide slide-2" data-delay="14">

                <!-- slide background -->
                <img src="masterslider/style/blank.gif" data-src="images/bg-3.jpg" alt="Slide1 background"/>
                <h3 class="ms-layer sub-title2 full-wid text-center text-uppercase"
                    style="left:0; top:200px;"
                    data-type="text"
                    data-delay="1000"
                    data-duration="2000"
                    data-ease="easeOutExpo"
                    data-effect="rotate3dtop(-100,0,0,40,t)">Interior  -  Construction  -  Renovation</h3>
                <h3 class="ms-layer title2 one full-wid font-sbold white text-uppercase text-center"
                    style="left:0;top: 250px;"
                    data-type="text"
                    data-delay="1500"
                    data-duration="2500"
                    data-ease="easeOutExpo"
                    data-effect="rotate3dtop(-100,0,0,40,t)">We offer Multi Services</h3>
                <h5 class="ms-layer sub-title2 full-wid white text-uppercase text-center"
                    style="left:0px; top: 330px;"
                    data-type="text"
                    data-effect="bottom(45)"
                    data-duration="3000"
                    data-delay="2000"
                    data-ease="easeOutExpo">Development dolor sit amet consectetur adipiscing Phasellus</h5>
                <a class="ms-layer btn1 uppercase" href="#"
                   style="left: 530px; top:380px;"
                   data-type="text"
                   data-delay="2500"
                   data-ease="easeOutExpo"
                   data-duration="2000"
                   data-effect="scale(1.5,1.6)"> Learn More <i class="ion-ios-arrow-right"></i> </a>
                <a class="ms-layer btn2 uppercase" href="#"
                   style="left: 700px; top:380px;"
                   data-type="text"
                   data-delay="3000"
                   data-ease="easeOutExpo"
                   data-duration="2200"
                   data-effect="scale(1.5,1.6)"> Purchase Now! <i class="ion-ios-arrow-right"></i></a> </div>
            <!-- end of slide -->
        </div>
    </div>
    <!-- end Master Slider -->



    <!--call to action-->
    <div class="cta">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 wow animated fadeInDown" data-wow-delay="0.2s">
                    <h3>Do you need Professionals to build your dream home?</h3>
                    <p>We offer the best home services lorem ipsum dolor sit amet</p>
                </div>
                <div class="col-lg-3 text-center wow animated fadeInUp" data-wow-delay="0.4s">
                    <a href="#" class=" btn btn-skin btn-xl">Contact us <i class="ion-em"></i></a>
                </div>
            </div>
        </div>
    </div>
    <!--end call to action-->
    <div class="space80"></div>
    <div class="container">
        <div class="row vertical-align-child">
            <div class="col-lg-5 hidden-xs wow animated fadeInUp" data-wow-delay="0.3s">
                <img src="images/single-1.png" alt="" class="img-fluid">
            </div>
            <div class="col-lg-6 ml-auto wow animated fadeInUp" data-wow-delay="0.6s">
                <h1>We are <span class="color_text">PASSIONATE</span> about our work</h1>
                <div class="border-width"></div>
                <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                </p>
                <hr>
                <div class="buttons">
                    <a href="#" class="btn btn-xl btn-skin">Our Services</a>
                    <a href="#" class="btn btn-xl btn-dark">Contact Us</a>
                </div>
            </div>
        </div>
        <div class="space40"></div>
        <div class="row">
            <div class="col-lg-4 margin-btm-30 text-center">
                <div class="icon-box clearfix wow animated fadeIn" data-wow-delay="0.3s">
                    <i class="flaticon-excavator-machine-arm"></i>
                    <div class="content">
                        <h4 class="text-uppercase">15 Years of experience</h4>
                        <div class="border-width"></div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                    </div>
                </div>
            </div><!--end col-->
            <div class="col-lg-4 margin-btm-30 text-center">
                <div class="icon-box clearfix wow animated fadeIn" data-wow-delay="0.6s">
                    <i class="flaticon-construction-crane-machine-1"></i>
                    <div class="content">
                        <h4 class="text-uppercase">Professionals</h4>
                        <div class="border-width"></div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                    </div>
                </div>
            </div><!--end col-->
            <div class="col-lg-4 margin-btm-30 text-center">
                <div class="icon-box clearfix wow animated fadeIn" data-wow-delay="0.9s">
                    <i class="flaticon-construction-excavator"></i>
                    <div class="content">
                        <h4 class="text-uppercase">Modern Technologies</h4>
                        <div class="border-width"></div>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                    </div>
                </div>
            </div><!--end col-->
        </div>
    </div>
    <!--end intro section-->
    <div class="space50"></div>
    <div class="gray-bg">
        <div class="space80"></div>
        <div class="container">
            <div class="center-title margin-btm-50">
                <h1>What We offer</h1>
                <div class="border-width center-align"></div>
            </div><!--center title end-->
            <div class="owl-carousel owl-theme service-slider">
                <div class="item">
                    <div class="service-box">
                        <div class="service-thumb">
                            <a href="#"><img src="images/s1.jpg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="service-desc">
                            <h4 class="text-uppercase">Interior Design</h4>
                            <div class="border-width"></div>
                            <p>
                                Sed ut perspiciatis unde omnis doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                            </p>
                            <div class="text-right">
                                <a href="#" class="btn btn-link">View Details</a>
                            </div>
                        </div>
                    </div>
                </div><!--services col end-->
                <div class="item">
                    <div class="service-box">
                        <div class="service-thumb">
                            <a href="#"><img src="images/s2.jpg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="service-desc">
                            <h4 class="text-uppercase">Project Management</h4>
                            <div class="border-width"></div>
                            <p>
                                Sed ut perspiciatis unde omnis doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                            </p>
                            <div class="text-right">
                                <a href="#" class="btn btn-link">View Details</a>
                            </div>
                        </div>
                    </div>
                </div><!--services col end-->
                <div class="item">
                    <div class="service-box">
                        <div class="service-thumb">
                            <a href="#"><img src="images/s3.jpg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="service-desc">
                            <h4 class="text-uppercase">House Renovation</h4>
                            <div class="border-width"></div>
                            <p>
                                Sed ut perspiciatis unde omnis doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                            </p>
                            <div class="text-right">
                                <a href="#" class="btn btn-link">View Details</a>
                            </div>
                        </div>
                    </div>
                </div><!--services col end-->
                <div class="item">
                    <div class="service-box">
                        <div class="service-thumb">
                            <a href="#"><img src="images/s4.jpg" alt="" class="img-fluid"></a>
                        </div>
                        <div class="service-desc">
                            <h4 class="text-uppercase">Constructions</h4>
                            <div class="border-width"></div>
                            <p>
                                Sed ut perspiciatis unde omnis doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                            </p>
                            <div class="text-right">
                                <a href="#" class="btn btn-link">View Details</a>
                            </div>
                        </div>
                    </div>
                </div><!--services col end-->
            </div><!--services row end-->
        </div>
        <div class="space50"></div>
        <div class="text-center">
            <a href="#" class="btn btn-xl btn-skin">View All Services <i class="ion-android-arrow-forward"></i></a>
        </div>
        <div class="space80"></div>
    </div>
    <!--end gray bg with services-->
    <div class="space80"></div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="center-title margin-btm-50">
                    <h1>Recent Projects</h1>
                    <div class="border-width center-align"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div id="js-filters-agency" class="cbp-l-filters-text">
            <div class="cbp-l-filters-text-sort">Sort by:</div>
            <div data-filter="*" class="cbp-filter-item-active cbp-filter-item">
                All <div class="cbp-filter-counter"></div>
            </div> |
            <div data-filter=".interior" class="cbp-filter-item">
                Interior <div class="cbp-filter-counter"></div>
            </div> |
            <div data-filter=".renovation" class="cbp-filter-item">
                Renovation <div class="cbp-filter-counter"></div>
            </div> |
            <div data-filter=".management" class="cbp-filter-item">
                Management <div class="cbp-filter-counter"></div>
            </div> |
            <div data-filter=".construction" class="cbp-filter-item">
                Construction <div class="cbp-filter-counter"></div>
            </div>
        </div><!--filter-->
    </div>
    <div id="js-grid-agency" class="cbp cbp-l-grid-agency">
        <div class="cbp-item interior">
            <a href="#">
                <div class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/work-1.jpg" data-cbp-src="images/work-1.jpg" alt="" width="380" height="250">
                    </div>
                </div>
                <div class="cbp-l-grid-agency-title">Interior</div>
                <div class="cbp-l-grid-agency-desc">By Bpress Construction</div>
            </a>
        </div><!--end item-->
        <div class="cbp-item renovation">
            <a href="#">
                <div class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/work-2.jpg" data-cbp-src="images/work-2.jpg" alt="" width="380" height="250">
                    </div>
                </div>
                <div class="cbp-l-grid-agency-title">Renovation</div>
                <div class="cbp-l-grid-agency-desc">By Bpress Construction</div>
            </a>
        </div><!--end item-->
        <div class="cbp-item management">
            <a href="#">
                <div class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/work-3.jpg" data-cbp-src="images/work-3.jpg" alt="" width="380" height="250">
                    </div>
                </div>
                <div class="cbp-l-grid-agency-title">Project Management</div>
                <div class="cbp-l-grid-agency-desc">By Bpress Construction</div>
            </a>
        </div><!--end item-->
        <div class="cbp-item construction">
            <a href="#">
                <div class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/work-4.jpg" data-cbp-src="images/work-4.jpg" alt="" width="380" height="250">
                    </div>
                </div>
                <div class="cbp-l-grid-agency-title">Construction</div>
                <div class="cbp-l-grid-agency-desc">By Bpress Construction</div>
            </a>
        </div><!--end item-->
        <div class="cbp-item interior">
            <a href="#">
                <div class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/work-5.jpg" data-cbp-src="images/work-5.jpg" alt="" width="380" height="250">
                    </div>
                </div>
                <div class="cbp-l-grid-agency-title">Interior</div>
                <div class="cbp-l-grid-agency-desc">By Bpress Construction</div>
            </a>
        </div><!--end item-->
        <div class="cbp-item renovation">
            <a href="#">
                <div class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/work-6.jpg" data-cbp-src="images/work-6.jpg" alt="" width="380" height="250">
                    </div>
                </div>
                <div class="cbp-l-grid-agency-title">Renovation</div>
                <div class="cbp-l-grid-agency-desc">By Bpress Construction</div>
            </a>
        </div><!--end item-->
        <div class="cbp-item management">
            <a href="#">
                <div class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/work-7.jpg" data-cbp-src="images/work-7.jpg" alt="" width="380" height="250">
                    </div>
                </div>
                <div class="cbp-l-grid-agency-title">Project Management</div>
                <div class="cbp-l-grid-agency-desc">By Bpress Construction</div>
            </a>
        </div><!--end item-->
        <div class="cbp-item construction">
            <a href="#">
                <div class="cbp-caption">
                    <div class="cbp-caption-defaultWrap">
                        <img src="images/work-8.jpg" data-cbp-src="images/work-8.jpg" alt="" width="380" height="250">
                    </div>
                </div>
                <div class="cbp-l-grid-agency-title">Construction</div>
                <div class="cbp-l-grid-agency-desc">By Bpress Construction</div>
            </a>
        </div><!--end item-->
    </div>

    <div class="space50"></div>
    <div class="text-center container">
        <a href="#" class="btn btn-xl btn-dark">View All Projects <i class="ion-android-arrow-forward"></i></a>
    </div>
    <div class="space80"></div>
    <div class="gray-bg">
        <div class="container">
            <div class="row vertical-align-child">
                <div class="col-lg-6 wow animated fadeInUp" data-wow-delay="0.2s">
                    <div class="space30"></div>
                    <h3 class="left-title">Why Choose us?</h3>
                    <div id="accordion" role="tablist">
                        <div class="card accordions-light">
                            <div class="card-header" role="tab" id="headingOne">
                                <h5 class="mb-0">
                                    <a data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        Latest Bootstrap4
                                    </a>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>
                        <div class="card accordions-light">
                            <div class="card-header" role="tab" id="headingTwo">
                                <h5 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Clean & well coded
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>
                        <div class="card accordions-light">
                            <div class="card-header" role="tab" id="headingThree">
                                <h5 class="mb-0">
                                    <a class="collapsed" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Minimal to large
                                    </a>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="space30"></div>
                </div>
                <div class="col-lg-6 wow animated fadeIn" data-wow-delay="0.4s">
                    <img src="images/single-2.png" alt="" class="img-fluid hidden-xs">
                </div>
            </div>
        </div>
    </div>

    <div class="testimonials">
        <div class="container">
            <div class="center-title margin-btm-50">
                <h1>What they say</h1>
                <div class="border-width center-align"></div>
            </div>
            <div class="row">
                <div class="col-lg-8 mr-auto ml-auto text-center">
                    <div class="testi-slider owl-carousel owl-theme">
                        <div class="item">
                            <i class="ion-quote"></i>
                            <p>
                                " We are so happy working with Bpress, the guys are very familiar and work with passionate, services are absolutely awesome, highly recommended. "
                            </p>
                            <h5>John Doe</h5>
                        </div>
                        <div class="item">
                            <i class="ion-quote"></i>
                            <p>
                                " We are so happy working with Bpress, the guys are very familiar and work with passionate, services are absolutely awesome, highly recommended. "
                            </p>
                            <h5>John Doe</h5>
                        </div>
                        <div class="item">
                            <i class="ion-quote"></i>
                            <p>
                                " We are so happy working with Bpress, the guys are very familiar and work with passionate, services are absolutely awesome, highly recommended. "
                            </p>
                            <h5>John Doe</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--end testimonials-->
    <div class="space80"></div>
    <div class="container">
        <div class="center-title margin-btm-50">
            <h1>Company News</h1>
            <div class="border-width center-align"></div>
        </div>
        <div class="row">
            <div class="col-lg-9 margin-btm-30">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="post wow animated fadeInUp" data-wow-delay="0.2s">
                            <a href="post.html">
                                <img src="images/s1.jpg" alt="" class="img-fluid">
                            </a>
                            <div class="space20"></div>
                            <div class="post-meta">
                                <h4><a href="#">Post Title goes here</a></h4>
                                <ul class="list-inline">
                                    <li class="list-inline-item"><a href="#"><i class="ion-person"></i> User</a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ion-ios-clock"></i> 12 May 2016</a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ion-ios-chatbubble"></i> 3 Comments</a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ion-pound"></i> Interior</a></li>
                                </ul>
                            </div><!--post meta-->
                            <div class="post-content">
                                <p>
                                    If you use this site regularly and would like to help keep the site on the Internet, please consider donating a small sum to help pay for the hosting and bandwidth bill. There is no minimum donation
                                </p>
                                <p class="text-right">
                                    <a href="#" class="btn btn-link">Read More...</a>
                                </p>
                            </div>
                        </div><!--post end-->
                    </div>
                    <div class="col-lg-6">
                        <div class="post wow animated fadeInUp" data-wow-delay="0.4s">
                            <a href="post.html">
                                <img src="images/s2.jpg" alt="" class="img-fluid">
                            </a>
                            <div class="space20"></div>
                            <div class="post-meta">
                                <h4><a href="#">Post Title goes here</a></h4>
                                <ul class="list-inline">
                                    <li class="list-inline-item"><a href="#"><i class="ion-person"></i> User</a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ion-ios-clock"></i> 12 May 2016</a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ion-ios-chatbubble"></i> 3 Comments</a></li>
                                    <li class="list-inline-item"><a href="#"><i class="ion-pound"></i> Interior</a></li>
                                </ul>
                            </div><!--post meta-->
                            <div class="post-content">
                                <p>
                                    If you use this site regularly and would like to help keep the site on the Internet, please consider donating a small sum to help pay for the hosting and bandwidth bill. There is no minimum donation
                                </p>
                                <p class="text-right">
                                    <a href="#" class="btn btn-link">Read More...</a>
                                </p>
                            </div>
                        </div><!--post end-->
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <ul class="card-list list-unstyled wow animated fadeInUp" data-wow-delay="0.6s">
                    <li>
                        <a href="post.html">
                            <h4>Voluptate velit esse quam nihil molestiae consequatur</h4>
                            <span>2 Days Ago <em>Interior</em></span>
                        </a>
                    </li>
                    <li>
                        <a href="post.html">
                            <h4>What the estimate time of house painting.</h4>
                            <span>2 Days Ago <em>Interior</em></span>
                        </a>
                    </li>
                    <li>
                        <a href="post.html">
                            <h4>Voluptate velit esse quam nihil molestiae consequatur</h4>
                            <span>2 Days Ago <em>Interior</em></span>
                        </a>
                    </li>
                    <li>
                        <a href="blog.html" class="btn btn-link">
                            Read All news
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="space50"></div>

    <div class="c_info_bar wow animated fadeIn" data-wow-delay="0.3s">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center margin-btm-30">
                    <h4><i class="ion-ios-time"></i> Monday - Saturday 9:Am - 6:Pm / Sunday: Closed</h4>
                </div>
            </div>
        </div>
    </div>
    <!--footer-->
    `


@endsection