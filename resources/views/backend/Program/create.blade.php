@extends('backend.baselayout')
@section('admin-section')

    <div class="row">
        <h3> Create Sliders</h3>
    </div>
    <div class="col-md-8">

        <form method="post" action="" enctype="multipart/form-data">
            {{csrf_field()}}
            <h3>Title</h3>
            <h3><input type="text" name="title" placeholder="Enter title" class="form-control" /></h3>
            <h3>Heading</h3>
            <h3><input type="text" name="heading" placeholder="enter the heading" class="form-control"/></h3>
            <h3> Description</h3>
            <textarea name="description" rows="7" cols="50" class="form-control"></textarea>

            <h3> Image</h3>
            <input type="file" name="image" class="form-control">
            <br>
            <p><input type="submit" name="submit" value="Create sliders" class="btn btn-primary"/></p>

        </form>

    </div>

@endsection

