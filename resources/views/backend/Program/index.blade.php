@extends('backend.baselayout')
@section('admin-section')

<div class="row">
    <h3> Create Sliders</h3>
</div>
<div class="col-md-12">

    <!--Table-->
    <table class="table">

        <!--Table head-->
        <thead class="blue-grey lighten-4">
        <tr>
            <th>#</th>
            <th> Name</th>
            <th>uni Name</th>
            <th>status</th>
        </tr>
        </thead>
        <!--Table head-->

        <!--Table body-->
        <tbody>
        <tr>
            <th scope="row">1</th>
            <td>bio tech</td>
            <td>civil</td>
            <td>comp</td>
        </tr>
        <tr>
            <th scope="row">2</th>
            <td>civil</td>
            <td>Thornton</td>
            <td>@fat</td>
        </tr>
        <tr>
            <th scope="row">3</th>
            <td>comp</td>
            <td>the Bird</td>
            <td>@twitter</td>
        </tr>
        </tbody>
        <!--Table body-->

    </table>
    <!--Table-->


</div>

    @endsection