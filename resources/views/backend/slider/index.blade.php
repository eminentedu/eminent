@extends('backend.baselayout');
@section('admin-section')
    <div class="row">
        <h3> List of Sliders</h3>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        DataTables Advanced Tables
                    </div>
                    <!-- /.panel-heading -->
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>SN</th>
                                    <th>Title</th>
                                    <th>Heading</th>
                                    <th>Descripton</th>
                                    <th>Image</th>
                                    <th>Status</th>
                                    <th>Action(s)</th>

                                </tr>
                                </thead>
                                <tbody>

                                @foreach($sliders as $slider)
                                    <tr>
                                        <td>1</td>
                                        <td>{{ $slider->title }}</td>
                                        <td> {{ $slider->heading }}</td>
                                        <td>{!! $slider->description !!}</td>
                                        <td></td>
                                        <td>
                                            {{ $slider->status==1?"active":"inactive" }}</td>
                                        <td>
                                            <a href="{{ url('admin/slider/'.$slider->id.'/edit') }}"><button type="button" class="btn btn-primary"> <span class="fa fa-edit"></span> </button></a>
                                            <form action="{{ url('admin/slider/'.$slider->id) }}" method="POST">
                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                <input type="hidden" name="_method" value="Delete">
                                                <input type="submit" class="btn btn-danger" value="Delete" onclick="return confirm('Are you sure to Delete')">

                                            </form>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>




    </div>
@endsection