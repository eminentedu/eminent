@extends('backend.baselayout');
@section('admin-section')

        <div class="row">
            <h3> Create Sliders</h3>
        </div>
            <div class="col-md-8">

                <form method="post" action="{{ route('admin.slider.store') }}" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label class="control-label" for="inputSuccess">Title</label>
                        <input type="text" class="form-control" name="title">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="inputSuccess">Heading</label>
                        <input type="text" class="form-control" name="heading">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="inputSuccess">Description</label>
                        <textarea name="description" rows="7" cols="50" class="form-control"></textarea>
                    </div>
                    <div class="form-group">
                         <label class="control-label" for="inputSuccess">Choose</label>
                        <input type="file" name="image" class="form-control">
                    </div>

                    <div class="form-group">
                        <label class="control-label" for="inputSuccess">Choose Status</label>
                            <select class="form-control" name="status">
                                <option value="1">Active</option>
                                <option value="0'">Inactive</option>
                            </select>
                    </div>
                    <br>
                    <p><input type="submit" name="submit" class="btn btn-primary"/></p>

                </form>
            </div>

    @endsection

