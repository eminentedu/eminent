@extends('backend.baselayout');
@section('admin-section')

    <div class="row">
        <h3> Edit Sliders</h3>
    </div>
    <div class="col-md-8">

        <form method="post" action="{{ url('admin/slider/'.$sliders->id) }}" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="_method" value="PATCH">
            <div class="form-group">
                <label class="control-label" for="inputSuccess">Title</label>
                <input type="text" class="form-control" value="{{ $sliders->title }}" name="title">
            </div>
            <div class="form-group">
                <label class="control-label" for="inputSuccess">Heading</label>
                <input type="text" class="form-control" value="{{ $sliders->heading }}" name="heading">
            </div>
            <div class="form-group">
                <label class="control-label" for="inputSuccess">Description</label>
                <textarea name="description" rows="7" cols="50" class="form-control">
                    {{ $sliders->description }}
                </textarea>
            </div>
            <div class="form-group">
                <label class="control-label" for="inputSuccess">Choose</label>
                <input type="file" name="image" class="form-control">
            </div>

            <div class="form-group">
                <label class="control-label" for="inputSuccess">Choose Status</label>
                <select class="form-control" name="status">
                    <option value="1">Active</option>
                    <option value="0'">Inactive</option>
                </select>
            </div>
            <br>
            <p><input type="submit" name="submit" class="btn btn-primary"/></p>

        </form>
    </div>

@endsection

