<?php

namespace App\Http\Controllers\Admin;

use App\Model\Slider;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $sliders=Slider::where('status',1)->get();
        return view('backend.slider.index')
            ->with('sliders',$sliders);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request->all());
        $slider = Slider::create(Input::all());
        if (Input::hasFile('image'))
        {
            $image= Input::file('image');
            $imageName=Str::random('32').'.'.$image->getClientOriginalExtension();
            $image->move('public/images/slider/',$imageName);
            $slider->image=$imageName;
        }
        $slider->save();
        return redirect('admin/slider');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sliders=Slider::where('status',1)->first();
        return view('backend.slider.edit')
            ->with('sliders',$sliders);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $slider=Slider::find($id);
        $slider->title=Input::get('title');
        $slider->heading=Input::get('heading');
        $slider->description=Input::get('description');
        $slider->status=Input::get('status');
        if (Input::hasFile('image'))
        {
            $image= Input::file('image');
            $imageName=Str::random('32').'.'.$image->getClientOriginalExtension();
            $image->move('public/images/slider/',$imageName);
            $slider->image=$imageName;
        }
        $slider->save();
        return redirect('admin/slider');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slider=Slider::find($id);
        $slider->delete();
        return redirect('admin/slider');
    }
}
