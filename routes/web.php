<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/','Frontend\HomeController@index');
Route::get('/member','Frontend\HomeController@ourteam');
Route::get('/about','Frontend\HomeController@about');
Route::get('/eminent-education','Frontend\HomeController@eminentedu');
Route::get('/biotech-usa','Frontend\HomeController@biotech');
Route::get('/uk-uni','Frontend\HomeController@ukuni');






Route::group(['prefix'=>'admin' ,'as' => 'admin.','middleware'=>'auth'],function() {
    Route::get('/', 'admin\AdminController@dashboard')->name('dashboard');
    Route::resource('/slider', 'admin\SliderController');
    Route::resource('/program', 'admin\ProgramController');


});




Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

